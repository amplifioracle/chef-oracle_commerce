name             'oracle_commerce'
maintainer       'Amplifi Commerce'
maintainer_email 'brian.celenza@amplificommerce.com'
license          'All rights reserved'
description      'Installs/Configures Oracle Commerce, ATG and Endeca'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '11.1.0'

depends 'java'
depends 'zip'