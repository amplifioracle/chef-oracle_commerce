oracle_commerce Cookbook
========================
This cookbook installs the full oracle commerce suite, or any individual component, as desired. Installation may take some time due to the requirement to download the installers.

At this time configuring CIM from the cookbook is not setup, but should be investigated in the future.

Requirements
------------
Oracle Commerce requires Oracle Java (NOT OpenJDK) to install and configure. As of version 11, JDK 1.7 is required. The installer files must also be at an accessable URL from the server installing

Oracle Commerce Supported Versions
------------

* Oracle Commerce 11.0.0
* Oracle Commerce 11.0.1
* Oracle Commerce 11.1.0

Attributes
----------

### oracle_commerce::default

**\['oracle_commerce'\]\['base_path'\]** (String)
Where Oracle Commerce will be installed (*Default*: /app/oracle/product)

**\['oracle_commerce'\]\['user'\]** (String)
The unix user to install as (*No Default*)

**\['oracle_commerce'\]\['group'\]** (String)
The unix group to install as (*No Default*)


### oracle_commerce::mdex

**\['oracle_commerce'\]\['mdex'\]\['version'\]** (String)
The version of the mdex being installed (*No Default*)

**\['oracle_commerce'\]\['mdex'\]\['url'\]** (String)
The remote installer (*No Default*)

**\['oracle_commerce'\]\['mdex'\]\['checksum'\]** (String)
The SHA256 hash of the remote installer (*No Default*)


### oracle_commerce::platform_services

**\['oracle_commerce'\]\['platform_services'\]\['version'\]** (String)
The version of PlatformServices being installed (*No Default*)

**\['oracle_commerce'\]\['platform_services'\]\['url'\]** (String)
The remote installer (*No Default*)

**\['oracle_commerce'\]\['platform_services'\]\['checksum'\]** (String)
The SHA256 hash of the remote installer (*No Default*)

**\['oracle_commerce'\]\['platform_services'\]\['http_port'\]** (String)
The port on which the Endeca HTTP Service listens (*Default*: 8888)

**\['oracle_commerce'\]\['platform_services'\]\['shutdown_port'\]** (String)
The shutdown port for the Endeca HTTP Service (*Default*: 8090)
    
**\['oracle_commerce'\]\['platform_services'\]\['install_eac'\]** (String)
Indicates that you want to install EAC Central Server and Agent (Y/N, *Default*: Y)

**\['oracle_commerce'\]\['platform_services'\]\['install_references'\]** (String)
Indicates that you want to install the reference implementations (Y/N, *Default*: Y)


### oracle_commerce::tools_frameworks

**\['oracle_commerce'\]\['tools_frameworks'\]\['version'\]** (String)
The version of the Tools and Frameworks (Experience Manager) being installed (*No Default*)

**\['oracle_commerce'\]\['tools_frameworks'\]\['url'\]** (String)
The remote installer (*No Default*)

**\['oracle_commerce'\]\['tools_frameworks'\]\['checksum'\]** (String)
The SHA256 hash of the remote installer (*No Default*)

**\['oracle_commerce'\]\['tools_frameworks'\]\['admin_password'\]** (String)
The default administrator password (*Default*: admin)

**\['oracle_commerce'\]\['tools_frameworks'\]\['oracle_home'\]** (String)
The folder name to install under \['oracle_commerce'\]\['base_path'\]/endeca (*Default*: ToolsAndFrameworks)


### oracle_commerce::cas

**\['oracle_commerce'\]\['cas'\]\['version'\]** (String)
The version of CAS being installed (*No Default*)

**\['oracle_commerce'\]\['cas'\]\['url'\]** (String)
The remote installer (*No Default*)

**\['oracle_commerce'\]\['cas'\]\['checksum'\]** (String)
The SHA256 hash of the remote installer (*No Default*)

**\['oracle_commerce'\]\['cas'\]\['port'\]** (String)
The CAS listener port (*Default*: 8500)

**\['oracle_commerce'\]\['cas'\]\['shutdown_port'\]** (String)
The CAS shutdown port (*Default*: 8506)

**\['oracle_commerce'\]\['cas'\]\['host'\]** (String)
The CAS server FQDN (*Default*: localhost)


### oracle_commerce::platform

**\['oracle_commerce'\]\['platform'\]\['version'\]** (String)
The version of the platform (ATG) being installed (*Default*)

**\['oracle_commerce'\]\['platform'\]\['url'\]** (String)
The remote installer (*No Default*)

**\['oracle_commerce'\]\['platform'\]\['checksum'\]** (String)
The SHA256 hash of the remote installer (*No Default*)

**\['oracle_commerce'\]\['platform'\]\['path'\]** (String)
The path to install the platform (*Default*: \['oracle_commerce'\]\['base_path'\]/atg)

**\['oracle_commerce'\]\['platform'\]\['feature_list'\]** (String)
A comma separated list of features to install (RM=Core Platform, Commerce=Core Commerce and Merchandising, Portal=ATG Portal, CA=Content Administration, MP=Motorprise, QF=Quincy Funds) (*Default*: RM,Commerce,Portal,CA,MP,QF)


### oracle_commerce::crs

**\['oracle_commerce'\]\['crs'\]\['version'\]** (String)
The version of CRS being installed (*No Default*)

**\['oracle_commerce'\]\['crs'\]\['url'\]** (String)
The remote installer (*No Default*)

**\['oracle_commerce'\]\['crs'\]\['checksum'\]** (String)
The SHA256 hash of the remote installer (*No Default*)

### oracle_commerce::csc

**\['oracle_commerce'\]\['csc'\]\['version'\]** (String)
The version of CSC being installed (*No Default*)

**\['oracle_commerce'\]\['csc'\]\['url'\]** (String)
The remote installer (*No Default*)

**\['oracle_commerce'\]\['csc'\]\['checksum'\]** (String)
The SHA256 hash of the remote installer (*No Default*)

### oracle_commerce::cim

**\['oracle_commerce'\]\['cim'\]\['batch_path'\]** (String)
The path to the batch file to use for CIM (*No Default*)


Usage
-----
#### oracle_commerce

Just include `oracle_commerce` in your node's `run_list`, changing attributes above where applicable:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[oracle_commerce]"
  ]
}
```

License and Authors
-------------------
Authors: Brian Celenza <brian.celenza@amplificommerce.com>
