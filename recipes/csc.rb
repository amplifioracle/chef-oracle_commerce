#
# Cookbook Name:: oracle_commerce
# Recipe:: crcscs
#
# Copyright 2014, Amplifi Commerce
#
# All rights reserved - Do Not Redistribute
#
Chef::Log.info("Installing Oracle Commerce Service Center #{node['oracle_commerce']['csc']['version']}")

src_filename = "OCServiceCenter-#{node['oracle_commerce']['csc']['version']}.bin"
src_filepath = "#{Chef::Config[:file_cache_path]}/#{src_filename}"

# download the installer
remote_file src_filename do
  path src_filepath
  source node['oracle_commerce']['csc']['url']
  checksum node['oracle_commerce']['csc']['checksum']
  mode 0755
  action :create_if_missing
  retries 3
end

# install the silent template
silent_properties_filepath = "#{Chef::Config[:file_cache_path]}/installer.csc.properties"
template silent_properties_filepath do
  source "installer.csc.properties.erb"
  action :create
end

# unzip and run the installer silently
bash 'install_csc' do
  cwd ::File.dirname(src_filepath)
  code <<-EOH
    mkdir -p #{node['oracle_commerce']['base_path']}
    #{src_filepath} -f #{silent_properties_filepath} -i silent
    chown -R #{node['oracle_commerce']['user']}:#{node['oracle_commerce']['group']} #{node['oracle_commerce']['base_path']}
    EOH
end