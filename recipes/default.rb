include_recipe "oracle_commerce::mdex" unless node['oracle_commerce']['mdex']['url'].empty?
include_recipe "oracle_commerce::platform_services" unless node['oracle_commerce']['platform_services']['url'].empty?
include_recipe "oracle_commerce::tools_frameworks" unless node['oracle_commerce']['tools_frameworks']['url'].empty?
include_recipe "oracle_commerce::cas" unless node['oracle_commerce']['cas']['url'].empty?
include_recipe "oracle_commerce::platform" unless node['oracle_commerce']['platform']['url'].empty?
include_recipe "oracle_commerce::crs" unless node['oracle_commerce']['crs']['url'].empty?
include_recipe "oracle_commerce::csc" unless node['oracle_commerce']['csc']['url'].empty?
include_recipe "oracle_commerce::cim" unless node['oracle_commerce']['cim']['batch_path'].empty?