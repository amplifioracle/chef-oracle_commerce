#
# Cookbook Name:: oracle_commerce
# Recipe:: cas
#
# Copyright 2014, Amplifi Commerce
#
# All rights reserved - Do Not Redistribute
#
Chef::Log.info("Installing Oracle Commerce CAS #{node['oracle_commerce']['cas']['version']}")

src_filename = "OCCAS-#{node['oracle_commerce']['cas']['version']}.sh"
src_filepath = "#{Chef::Config[:file_cache_path]}/#{src_filename}"

# download the installer
remote_file src_filename do
  path src_filepath
  source node['oracle_commerce']['cas']['url']
  checksum node['oracle_commerce']['cas']['checksum']
  mode 0755
  action :create_if_missing
  retries 3
end

# install the silent template
silent_properties_filepath = "#{Chef::Config[:file_cache_path]}/silent.cas.txt"
template silent_properties_filepath do
  source "silent.cas.txt.erb"
  action :create
end

# unzip and run the installer silently
bash 'install_cas' do
  cwd ::File.dirname(src_filepath)
  user node['oracle_commerce']['user']
  group node['oracle_commerce']['group']
  code <<-EOH
    rm -rf #{node['oracle_commerce']['base_path']}/endeca/CAS
    #{src_filepath} --silent --target #{node['oracle_commerce']['base_path']} < #{silent_properties_filepath}
    EOH
end
