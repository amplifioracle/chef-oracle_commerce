#
# Cookbook Name:: oracle_commerce
# Recipe:: platform
#
# Copyright 2014, Amplifi Commerce
#
# All rights reserved - Do Not Redistribute
#
Chef::Log.info("Installing Oracle Commerce Platform #{node['oracle_commerce']['platform']['version']}")

src_filename = "OCPlatform-#{node['oracle_commerce']['platform']['version']}.bin"
src_filepath = "#{Chef::Config[:file_cache_path]}/#{src_filename}"

# download the installer
remote_file src_filename do
  path src_filepath
  source node['oracle_commerce']['platform']['url']
  checksum node['oracle_commerce']['platform']['checksum']
  mode 0755
  action :create_if_missing
  retries 3
end

# install the silent template
silent_properties_filepath = "#{Chef::Config[:file_cache_path]}/installer.atg.properties"
template silent_properties_filepath do
  source "installer.atg.properties.erb"
  action :create
end

# unzip and run the installer silently
bash 'install_platform' do
  cwd ::File.dirname(src_filepath)
  code <<-EOH
    mkdir -p #{node['oracle_commerce']['base_path']}
    #{src_filepath} -f #{silent_properties_filepath} -i silent
    chown -R #{node['oracle_commerce']['user']}:#{node['oracle_commerce']['group']} #{node['oracle_commerce']['base_path']}
    EOH
end

# add variables to env
ini_path = "/etc/profile.d/atg_ini.sh"
template ini_path do
  source "atg_ini.sh.erb"
  action :create
end