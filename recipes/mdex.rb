#
# Cookbook Name:: oracle_commerce
# Recipe:: mdex
#
# Copyright 2014, Amplifi Commerce
#
# All rights reserved - Do Not Redistribute
#
Chef::Log.info("Installing Oracle Commerce MDEX #{node['oracle_commerce']['mdex']['version']}")

src_filename = "OCMDEX-#{node['oracle_commerce']['mdex']['version']}.sh"
src_filepath = "#{Chef::Config[:file_cache_path]}/#{src_filename}"

# download the installer
remote_file src_filename do
  path src_filepath
  source node['oracle_commerce']['mdex']['url']
  checksum node['oracle_commerce']['mdex']['checksum']
  mode 0755
  action :create_if_missing
  retries 3
end

# unzip and run the installer silently
bash 'install_mdex' do
  cwd ::File.dirname(src_filepath)
  code <<-EOH
    mkdir -p #{node['oracle_commerce']['base_path']}
    rm -rf #{node['oracle_commerce']['base_path']}/endeca/MDEX
    chown -R #{node['oracle_commerce']['user']}:#{node['oracle_commerce']['group']} #{node['oracle_commerce']['base_path']}
    su #{node['oracle_commerce']['user']} -c "#{src_filepath} --silent --target #{node['oracle_commerce']['base_path']}"
    EOH
end

# add env vars to profile
ini_path = "#{node['oracle_commerce']['base_path']}/endeca/MDEX/#{node['oracle_commerce']['mdex']['version']}/mdex_setup_sh.ini"
bash 'mdex_env_vars' do
  cwd ::File.dirname(src_filepath)
  code <<-EOH
    cp #{ini_path} /etc/profile.d/mdex_setup_sh.sh
    source #{ini_path}
    EOH
end