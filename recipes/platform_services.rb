#
# Cookbook Name:: oracle_commerce
# Recipe:: platform_services
#
# Copyright 2014, Amplifi Commerce
#
# All rights reserved - Do Not Redistribute
#
Chef::Log.info("Installing Oracle Commerce Platform Services #{node['oracle_commerce']['platform_services']['version']}")

src_filename = "OCPlatformServices-#{node['oracle_commerce']['platform_services']['version']}.sh"
src_filepath = "#{Chef::Config[:file_cache_path]}/#{src_filename}"

# download the installer
remote_file src_filename do
  path src_filepath
  source node['oracle_commerce']['platform_services']['url']
  checksum node['oracle_commerce']['platform_services']['checksum']
  mode 0755
  action :create_if_missing
  retries 3
end

# install the silent template
silent_properties_filepath = "#{Chef::Config[:file_cache_path]}/silent.platform_services.txt"
template silent_properties_filepath do
  source "silent.platform_services.txt.erb"
  action :create
end

# unzip and run the installer silently
bash 'install_platform_services' do
  cwd ::File.dirname(src_filepath)
  code <<-EOH
    mkdir -p #{node['oracle_commerce']['base_path']}
    rm -rf #{node['oracle_commerce']['base_path']}/endeca/PlatformServices
    chown -R #{node['oracle_commerce']['user']}:#{node['oracle_commerce']['group']} #{node['oracle_commerce']['base_path']}
    su #{node['oracle_commerce']['user']} -c "#{src_filepath} --silent --target #{node['oracle_commerce']['base_path']} < #{silent_properties_filepath}"
    EOH
end

# add env vars to profile
ini_path = "#{node['oracle_commerce']['base_path']}/endeca/PlatformServices/workspace/setup/installer_sh.ini"
bash 'platform_services_env_vars' do
  cwd ::File.dirname(src_filepath)
  code <<-EOH
    cp #{ini_path} /etc/profile.d/installer_sh.sh
    source #{ini_path}
    EOH
end