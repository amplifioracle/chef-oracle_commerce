#
# Cookbook Name:: oracle_commerce
# Recipe:: crs
#
# Copyright 2014, Amplifi Commerce
#
# All rights reserved - Do Not Redistribute
#
Chef::Log.info("Installing Oracle Commerce Reference Store #{node['oracle_commerce']['crs']['version']}")

src_filename = "OCReferenceStore-#{node['oracle_commerce']['crs']['version']}.bin"
src_filepath = "#{Chef::Config[:file_cache_path]}/#{src_filename}"

# download the installer
remote_file src_filename do
  path src_filepath
  source node['oracle_commerce']['crs']['url']
  checksum node['oracle_commerce']['crs']['checksum']
  mode 0755
  action :create_if_missing
  retries 3
end

# install the silent template
silent_properties_filepath = "#{Chef::Config[:file_cache_path]}/installer.crs.properties"
template silent_properties_filepath do
  source "installer.crs.properties.erb"
  action :create
end

# unzip and run the installer silently
bash 'install_crs' do
  cwd ::File.dirname(src_filepath)
  code <<-EOH
    mkdir -p #{node['oracle_commerce']['base_path']}
    rm -rf #{node['oracle_commerce']['platform']['path']}/CommerceReferenceStore
    #{src_filepath} -f #{silent_properties_filepath} -i silent
    chown -R #{node['oracle_commerce']['user']}:#{node['oracle_commerce']['group']} #{node['oracle_commerce']['base_path']}
    EOH
end