users = node['oracle_commerce']['user']

if !users.empty?
  # make sure its an array
  if !users.kind_of?(Array)
    users = [users]
  end

  # install a dekstop shortcut for each user
  users.each do |user|
    desktop_path = "/home/#{user}/Desktop"
    shortcut_path = "#{desktop_path}/ATG Control Center.desktop"
    directory desktop_path do
      owner user
      group user
      mode '0755'
      action :create
      recursive true
    end

    template shortcut_path do
      source "ATG Control Center.desktop.erb"
      action :create
      owner user
      group user
      mode "0755"
    end
  end
end
