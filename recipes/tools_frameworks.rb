#
# Cookbook Name:: oracle_commerce
# Recipe:: tools_frameworks
#
# Copyright 2014, Amplifi Commerce
#
# All rights reserved - Do Not Redistribute
#
Chef::Log.info("Installing Oracle Commerce Tools And Frameworks #{node['oracle_commerce']['tools_frameworks']['version']}")

src_filename = "OCToolsAndFrameworks-#{node['oracle_commerce']['tools_frameworks']['version']}.zip"
src_filepath = "#{Chef::Config[:file_cache_path]}/#{src_filename}"
extracted_filepath = "#{Chef::Config[:file_cache_path]}/tools_frameworks/cd/Disk1"

# download the installer
remote_file src_filename do
  path src_filepath
  source node['oracle_commerce']['tools_frameworks']['url']
  checksum node['oracle_commerce']['tools_frameworks']['checksum']
  mode 0755
  action :create_if_missing
  retries 3
end

# unzip the installer
bash 'extract_tools_frameworks' do
  cwd ::File.dirname(src_filepath)
  code <<-EOH
    mkdir -p #{node['oracle_commerce']['base_path']}
    chown -R #{node['oracle_commerce']['user']}:#{node['oracle_commerce']['group']} #{node['oracle_commerce']['base_path']}
    unzip -o #{src_filepath} -d tools_frameworks
    EOH
end

# install the silent template
silent_properties_filepath = "#{extracted_filepath}/install/silent_response.rsp"
template silent_properties_filepath do
  source "silent_response.rsp.erb"
  action :create
end

# change ownership of installer path
bash 'installer_path_ownership' do
  code "chown -R #{node['oracle_commerce']['user']}:#{node['oracle_commerce']['group']} #{extracted_filepath}"
end

# run the installer silently
execute 'install_tools_frameworks' do
  user node['oracle_commerce']['user']
  group node['oracle_commerce']['group']
  # Emptying the LC_ALL environment variable prevents a scenario where the password is not set properly on install
  # DO NOT REMOVE.
  environment ({ "LC_ALL" => "" })
  command "sh #{extracted_filepath}/install/silent_install.sh #{extracted_filepath}/install/silent_response.rsp #{node['oracle_commerce']['tools_frameworks']['oracle_home']} #{node['oracle_commerce']['base_path']}/endeca/#{node['oracle_commerce']['tools_frameworks']['oracle_home']} #{node['oracle_commerce']['tools_frameworks']['admin_password']}"
end

# add env vars to profile
ini_path = "/etc/profile.d/tools_frameworks_sh.sh"
template ini_path do
  source "tools_frameworks_sh.sh.erb"
  action :create
end

bash 'tools_frameworks_env_vars' do
  code <<-EOH
    source #{ini_path}
    EOH
end