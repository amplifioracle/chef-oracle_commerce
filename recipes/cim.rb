#
# Cookbook Name:: oracle_commerce
# Recipe:: cim
#
# Copyright 2014, Amplifi Commerce
#
# All rights reserved - Do Not Redistribute
#
Chef::Log.info("Running Oracle Commerce CIM")

# unzip and run the installer silently
bash 'run_cim' do
  cwd ::File.dirname(src_filepath)
  user node['oracle_commerce']['user']
  group node['oracle_commerce']['group']
  code <<-EOH
    $DYNAMO_HOME/bin/cim.sh -batch #{node['oracle_commerce']['cim']['batch_path']}
    EOH
end