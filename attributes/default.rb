# install path defaults
default['oracle_commerce']['base_path'] = "/app/oracle/product"
default['oracle_commerce']['user'] = ""
default['oracle_commerce']['group'] = ""

# MDEX
default['oracle_commerce']['mdex']['version'] = ""
default['oracle_commerce']['mdex']['url'] = ""
default['oracle_commerce']['mdex']['checksum'] = ""

# PlatformServices
default['oracle_commerce']['platform_services']['version'] = ""
default['oracle_commerce']['platform_services']['url'] = ""
default['oracle_commerce']['platform_services']['checksum'] = ""
default['oracle_commerce']['platform_services']['http_port'] = "8888"
default['oracle_commerce']['platform_services']['shutdown_port'] = "8090"
default['oracle_commerce']['platform_services']['install_eac'] = "Y"
default['oracle_commerce']['platform_services']['install_references'] = "Y"

# ToolsAndFrameworks
default['oracle_commerce']['tools_frameworks']['version'] = ""
default['oracle_commerce']['tools_frameworks']['url'] = ""
default['oracle_commerce']['tools_frameworks']['checksum'] = ""
default['oracle_commerce']['tools_frameworks']['admin_password'] = "admin"
default['oracle_commerce']['tools_frameworks']['oracle_home'] = "ToolsAndFrameworks"

# CAS
default['oracle_commerce']['cas']['version'] = ""
default['oracle_commerce']['cas']['url'] = ""
default['oracle_commerce']['cas']['checksum'] = ""
default['oracle_commerce']['cas']['port'] = "8500"
default['oracle_commerce']['cas']['shutdown_port'] = "8506"
default['oracle_commerce']['cas']['host'] = "localhost"

# platform 
default['oracle_commerce']['platform']['version'] = ""
default['oracle_commerce']['platform']['url'] = ""
default['oracle_commerce']['platform']['checksum'] = ""
default['oracle_commerce']['platform']['path'] = "#{default['oracle_commerce']['base_path']}/atg"
default['oracle_commerce']['platform']['feature_list'] = "RM,Commerce,Portal,CA,MP,QF"

# crs
default['oracle_commerce']['crs']['version'] = ""
default['oracle_commerce']['crs']['url'] = ""
default['oracle_commerce']['crs']['checksum'] = ""

# csc
default['oracle_commerce']['csc']['version'] = ""
default['oracle_commerce']['csc']['url'] = ""
default['oracle_commerce']['csc']['checksum'] = ""

# CIM
default['oracle_commerce']['cim']['batch_path'] = ""